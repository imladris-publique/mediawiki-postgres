<?php
# This file was automatically generated by the MediaWiki 1.40.1
# installer. If you make manual changes, please keep track in case you
# need to recreate them later.
#
# See includes/MainConfigSchema.php for all configurable settings
# and their default values, but don't forget to make changes in _this_
# file, not there.
#
# Further documentation for configuration settings may be found at:
# https://www.mediawiki.org/wiki/Manual:Configuration_settings

# Protect against web entry
if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

# ——————————————————————————————————————————————————————————————
#                          Sécurité
# ——————————————————————————————————————————————————————————————

function get_secret(string $nom, $defaut=NULL, $filter=false){
	/*
	Récupère le paramètre à partir des variables d'environnement.
	Récupère en priorité le secret docker si la variable _FILE est présente,
	sinon utilise la variable d'environnement, si présente.
	Si l'option filter est précisée, formate le résultat avant de le renvoyer.
	(cf https://www.php.net/manual/en/filter.filters.validate.php)
	Si aucune variable d'environnement n'est précisée, utilise la valeur par défaut. (à ne pas utiliser en prod)
	*/
	$nom_file = $nom.'_FILE';
	if( isset($_ENV[$nom_file]) )
				$retour = \trim(\file_get_contents($_ENV[$nom_file]));
	else 	$retour = $_ENV[$nom] ?? $defaut;
	return $filter ? filter_var($retour, $filter) : $retour;
}

$wgSecretKey = get_secret('WIKI_SECRET_KEY','2c7eed35794f9d91b9fdf02ebdce93ca083ae46b0c9025a8d2986a7b75e80890');

# Changing this will log out all existing sessions.
$wgAuthenticationTokenVersion = "1";

# Site upgrade key. Must be set to a string (default provided) to turn on the
# web installer while LocalSettings.php is in place
$wgUpgradeKey = get_secret('WIKI_UPGRADE_KEY','a48e2660baad2040');


# -------------------------- debugage --------------------------

$wgShowExceptionDetails = false;
$wgShowDBErrorBacktrace = false;
$wgShowSQLErrors = false;

# ——————————————————————————————————————————————————————————————

## Uncomment this to disable output compression
# $wgDisableOutputCompression = true;

$wgSitename = "Un_Super_Wiki";
$wgMetaNamespace = "Projet";

## The URL base path to the directory containing the wiki;
## defaults for all runtime URL paths are based off of this.
## For more information on customizing the URLs
## (like /w/index.php/Page_title to /wiki/Page_title) please see:
## https://www.mediawiki.org/wiki/Manual:Short_URL
$wgScriptPath = "";

## The protocol and server name to use in fully-qualified URLs
$wgServer = get_secret('WIKI_URL', "http://localhost:8082");

## The URL path to static resources (images, scripts, etc.)
$wgResourceBasePath = $wgScriptPath;

## The URL paths to the logo.  Make sure you change this from the default,
## or else you'll overwrite your logo when you upgrade!
$wgLogos = [
	'1x' => "$wgResourceBasePath/images/public/chat.png",
	'icon' => "$wgResourceBasePath/images/public/chat.png",
];




# ——————————————————————————————————————————————————————————————
#                          Mail
# ——————————————————————————————————————————————————————————————
## UPO means: this is also a user preference option

$wgEnableEmail = true; # https://www.mediawiki.org/wiki/Manual:$wgEnableEmail

$wgEnableUserEmail = true; # UPO

$wgEmergencyContact	= get_secret('WIKI_MAIL_ADMIN',     'admin@domaine.fr', FILTER_VALIDATE_EMAIL);
$wgPasswordSender		= get_secret('WIKI_MAIL_PW_SENDER', 'admin@domaine.fr', FILTER_VALIDATE_EMAIL);

$wgEnotifUserTalk = true; # UPO
$wgEnotifWatchlist = true; # UPO
$wgEmailAuthentication = true;

// https://www.mediawiki.org/wiki/Manual:$wgSMTP
$wgSMTP = [
  'host'      => get_secret('WIKI_SMTP_HOST',    'mail.example.com'),           // could also be an IP address. Where the SMTP server is located. If using SSL or TLS, add the prefix "ssl://" or "tls://".
  'IDHost'    => get_secret('WIKI_SMTP_ID',      'example.com'),                // Generally this will be the domain name of your website (aka mywiki.org)
  'localhost' => get_secret('WIKI_SMTP_ID',      'example.com'),                // Same as IDHost above; required by some mail servers
  'port'      => get_secret('WIKI_SMTP_PORT',    587,  FILTER_VALIDATE_INT),    // Port to use when connecting to the SMTP server
  'auth'      => get_secret('WIKI_SMTP_AUTH',    true, FILTER_VALIDATE_BOOLEAN),// Should we use SMTP authentication (true or false)
  'username'  => get_secret('WIKI_SMTP_USER',    'my_user_name'),               // Username to use for SMTP authentication (if being used)
  'password'  => get_secret('WIKI_SMTP_PASSWORD','my_password')                 // Password to use for SMTP authentication (if being used)
];

# ——————————————————————————————————————————————————————————————
#                          Database
# ——————————————————————————————————————————————————————————————

## Database settings ( Penser à les changer en prod )
$wgDBtype = "postgres";
$wgDBserver		= get_secret('WIKI_DB_URL',			'docker-db');
$wgDBname			= get_secret('WIKI_DB_NAME',		'bdd-wiki');
$wgDBuser 		= get_secret('WIKI_DB_USER',		'wiki_user');
$wgDBpassword	= get_secret('WIKI_DB_PASSWORD','mot de passe');

# Postgres specific settings
$wgDBport			= get_secret('WIKI_DB_PORT',		'5432');
$wgDBmwschema	= get_secret('WIKI_DB_SCHEMA',	'mediawiki');

# Shared database table
# This has no effect unless $wgSharedDB is also set.
$wgSharedTables[] = "actor";

## Shared memory settings
$wgMainCacheType = CACHE_ACCEL;
$wgMemCachedServers = [];



# Periodically send a pingback to https://www.mediawiki.org/ with basic data
# about this MediaWiki instance. The Wikimedia Foundation shares this data
# with MediaWiki developers to help guide future development efforts.
$wgPingback = true;

# Site language code, should be one of the list in ./includes/languages/data/Names.php
$wgLanguageCode = "fr";

# Time zone
$wgLocaltimezone = "UTC";

## Set $wgCacheDirectory to a writable directory on the web server
## to make your wiki go slightly faster. The directory should not
## be publicly accessible from the web.
#$wgCacheDirectory = "$IP/cache";


## For attaching licensing metadata to pages, and displaying an
## appropriate copyright notice / icon. GNU Free Documentation
## License and Creative Commons licenses are supported so far.
$wgRightsPage = ""; # Set to the title of a wiki page that describes your license/copyright
$wgRightsUrl = "https://creativecommons.org/publicdomain/zero/1.0/";
$wgRightsText = "Creative Commons Zero (domaine public)";
$wgRightsIcon = "$wgResourceBasePath/resources/assets/licenses/cc-0.png";

# Path to the GNU diff3 utility. Used for conflict resolution.
$wgDiff3 = "/usr/bin/diff3";


## Default skin: you can change the default skin. Use the internal symbolic
## names, e.g. 'vector' or 'monobook':
$wgDefaultSkin = "vector";

# Enabled skins.
# The following skins were automatically enabled:
wfLoadSkin( 'MinervaNeue' );
wfLoadSkin( 'MonoBook' );
wfLoadSkin( 'Timeless' );
wfLoadSkin( 'Vector' );


# ——————————————————————————————————————————————————————————————
#                         Téléversement
# ——————————————————————————————————————————————————————————————

$wgEnableUploads = true; // Autorise le téléversement

$wgUseImageMagick = true; // vignettes automatiques
$wgImageMagickConvertCommand = "/usr/bin/convert";

$wgAllowCopyUploads = true; // autorise le téléchargement à partir d'une url
$wgCopyUploadsFromSpecialUpload = true; // autorise ce téléchargement à partir d'url par une option sur la page

$wgCopyUploadsDomains = []; // permet de lister les domaines d'où on peut télécharger. peut utiliser les wilcard *


# --------- Taille des fichiers --------- #
/*
 	post_max_size et upload_max_filesize dans le php.ini
	sont les limites imposée à PHP que ne peux dépasser la config
*/
$wgMaxUploadSize = [
    '*' => 50 * 1024 * 1024, // 50 Mo
    'url' => 100 * 1024 * 1024, // 100 Mo pour les téléchargements à partir d'url
];
$wgUploadSizeWarning= 1024*1024*20; // avertissement si dépassement de cette limite.

/*
TODO :
- arriver à ranger les fichiers dans des urls innaccessible sans les connaîtres.
- ranger les fichiers dans un répertoire wiki différent de celui du site
- faire que l'accès depuis le site aux images téléchargées soit facile.

https://www.mediawiki.org/wiki/Manual:$wgLocalFileRepo
$wgLocalFileRepo = [
               'class' => LocalRepo::class,
               'name' => 'local',
               'directory' => $wgUploadDirectory,
               'scriptDirUrl' => $wgScriptPath,
               'url' => $wgUploadBaseUrl ? $wgUploadBaseUrl . $wgUploadPath : $wgUploadPath,
               'hashLevels' => $wgHashedUploadDirectory ? 2 : 0,
               'thumbScriptUrl' => $wgThumbnailScriptPath,
               'transformVia404' => !$wgGenerateThumbnailOnParse,
               'deletedDir' => $wgDeletedDirectory,
               'deletedHashLevels' => $wgHashedUploadDirectory ? 3 : 0
       ];
*/


$wgHashedUploadDirectory=true; // défaut(true) range les fichiers dans des répertoires issus du hash du nom du fichier.


# InstantCommons allows wiki to use images from https://commons.wikimedia.org
$wgUseInstantCommons = true;

// Liste blanche stricte, ou simple avertissement ?
$wgStrictFileExtensions = true; # par défaut true


/* ---------------- Liste blanche ----------------
Par défaut :
$wgFileExtensions = [ 'png', 'gif', 'jpg', 'jpeg', 'webp', ];
Téléverser des fichiers dont l'extension n'est pas dans cette liste renverra un message d'alerte.
https://www.mediawiki.org/wiki/Manual:$wgFileExtensions/fr

$wgTrustedMediaFormats = [
	MEDIATYPE_BITMAP, // Tous les formats bitmap
	MEDIATYPE_AUDIO, // Tous les formats d'audio
	MEDIATYPE_VIDEO, // tous les formats vidéo simples
	"image/svg+xml", // svg (uniquement nécessaire si le rendu en ligne de svg n’est pas pris en charge)
	"application/pdf", // fichiers PDF
	# "application/x-shockwave-flash", //film flash/shockwave
];
*/

$wgFileExtensions[] = 'pdf';
$wgFileExtensions = array_merge(
    $wgFileExtensions, [
        'md', 'mp3', 'odt', 'odf', 'ogg', 'oga', 'pdf', 'svg', 'tar', 'tar.gz', 'txt', 'zip'
    ]
);

$wgTrustedMediaFormats[] = 'application/tar';
$wgTrustedMediaFormats[] = 'application/tar+gzip';
$wgTrustedMediaFormats[] = 'application/zip';

/* ---------------- Liste noire ----------------

https://www.mediawiki.org/wiki/Manual:$wgProhibitedFileExtensions
https://www.mediawiki.org/wiki/Manual:$wgMimeTypeExclusions

Par défaut :
$wgProhibitedFileExtensions = [
	# HTML may contain cookie-stealing JavaScript and web bugs
	'html', 'htm', 'js', 'jsb', 'mhtml', 'mht', 'xhtml', 'xht',
	# PHP scripts may execute arbitrary code on the server
	'php', 'phtml', 'php3', 'php4', 'php5', 'phps', 'phar',
	# Other types that may be interpreted by some servers
	'shtml', 'jhtml', 'pl', 'py', 'cgi',
	# May contain harmful executables for Windows victims
	'exe', 'scr', 'dll', 'msi', 'vbs', 'bat', 'com', 'pif', 'cmd', 'vxd', 'cpl',
	# T341565
	'xml',
];

$wgMimeTypeExclusions = [
	# HTML may contain cookie-stealing JavaScript and web bugs
	'text/html',
	# Similarly with JavaScript itself
	'application/javascript', 'text/javascript', 'text/x-javascript', 'application/x-shellscript',
	# PHP scripts may execute arbitrary code on the server
	'application/x-php', 'text/x-php',
	# Other types that may be interpreted by some servers
	'text/x-python', 'text/x-perl', 'text/x-bash', 'text/x-sh', 'text/x-csh',
	# Client-side hazards on Internet Explorer
	'text/scriptlet', 'application/x-msdownload',
	# Windows metafile, client-side vulnerability on some systems
	'application/x-msmetafile',
	# Files that look like java files
	'application/java',
	# XML files generally - T341565
	'application/xml', 'text/xml',
];

If you wanted to allow html files to be uploaded:
$wgFileExtensions[] = 'html';
$wgProhibitedFileExtensions = array_diff( $wgProhibitedFileExtensions, ['html'] );
$wgMimeTypeExclusions = array_diff( $wgMimeTypeExclusions, ['text/html'] );

*/
# ——————————————————————————————————————————————————————————————
#                          Extensions
# ——————————————————————————————————————————————————————————————

#
# https://www.mediawiki.org/wiki/Extension:CategoryTree
wfLoadExtension( 'CategoryTree' );
$wgCategoryTreeAllowTag = true;
$wgCategoryTreeDynamicTag = true;
$wgCategoryTreeMaxDepth = 7;
$wgCategoryTreeSidebarRoot='Catégorie';
# options à utiliser pour l'affichage de l'arbre dans la barre latérale, dans un tableau
$wgCategoryTreeSidebarOptions=['mode'=>'all','hideroot'=>'on','hideprefix'=>'always'];

wfLoadExtension( 'Cite' );
$wgCiteBookReferencing = true; # Enables an experimental feature for sub-references when set to true, see the help page. Default value: false
#$wgCiteResponsiveReferences #Default setting for responsive display of references. When set to true, the references section will be displayed in multiple columns. See the usage documentation. Default value: true
#$wgCiteVisualEditorOtherGroup #When set to true, the Cite toolbar button can be moved under the Insert menu. This is used on Wikivoyage sites. See the original feature request. Default value: false

#wfLoadExtension( 'CiteThisPage' );
#wfLoadExtension( 'ConfirmEdit' );
#wfLoadExtension( 'Echo' );
#wfLoadExtension( 'Flow' );
#wfLoadExtension( 'Gadgets' );
#wfLoadExtension( 'ImageMap' );
wfLoadExtension( 'InputBox' );  # https://www.mediawiki.org/wiki/Extension:InputBox/fr
wfLoadExtension( 'Interwiki' );

#wfLoadExtension( 'Kartographer' );
#$wgKartographerMapServer; # Server providing the map tiles. This is the only setting that doesn't have a default. You must provide a value. https://wiki.openstreetmap.org/wiki/Tile_servers
#$wgKartographerStyles	= ["osm-intl", "osm"]; # Available map styles users can request from your $wgKartographerMapServer via mapstyle="…".
#$wgKartographerDfltStyle	= "osm-intl"; # Default map style to be used. Must be one of the values from $wgKartographerStyles.
#$wgKartographerSrcsetScales=[ 1.3, 1.5, 2, 2.6, 3 ]; # Set of allowed high-DPI pixelratios supported by your $wgKartographerMapServer, to be used in <img srcset="…"> and $wgKartographerMapServer URLs. Must be in incremental order. Doesn't need to start with 1, this happens automatically. Note that most tile servers don't support this at all and thus require setting this to an empty array.
#$wgKartographerUseMarkerStyle = false; # Allows Kartographer to extract marker styles from the GeoJSON to style a ‎<maplink> accordingly. Currently only the marker-color is considered.
#$wgKartographerWikivoyageMode = false; # Enables the group feature where different ‎<maplink> and ‎<mapframe> tags can access each others data via group="…" and show="…" attributes. See Help:Extension:Kartographer#Groups. Disabled by default. Meant to be enabled on Wikivoyage.
#$wgKartographerStaticMapframe = false; # Disables the module that turns ‎<mapframe> tags into interactive mini-maps that can be panned and zoomed without leaving the page. Static maps are recommended on high-traffic wikis where your KartographerMapServer couldn't keep up. Clicking ‎<maplink> and ‎<mapframe> tags to open a fullscreen interactive map is always possible. Requires a static map renderer like Kartotherian.
#$wgKartographerStaticFullWidth= 1024; # The actual width of static map images when a ‎<mapframe> uses the special value width="full". Relevant on wikis in static mode or when JavaScript is disabled. It's recommended to use one of the $wgImageLimits widths for consistency.
#$wgKartographerUsePageLanguage= true; # Use the language of the page instead of that of the language of the territory mapped. This sets the lang parameter of the tiles requested.
#$wgKartographerFallbackZoom= 13; # Fallback zoom value when no zoom="…" is given. Must be an integer between 0 (map shows the entire earth) and 19 (as close as possible). Currently only works with dynamic maps.
#$wgKartographerSimpleStyleMarkers=true; # Use an api to generate markers using the simplestyle-spec for features. Should be set to false for most applications outside WMF. Otherwise images of markers are assumed to be hosted on the map server.

#wfLoadExtension( 'LocalisationUpdate' );
#wfLoadExtension( 'NamespaceRelations' ); # Pour faire des onglets par namespace
#wfLoadExtension( 'MultimediaViewer' );
#wfLoadExtension( 'MyVariables' ); # introduit des magicwords comme {{SUBPAGES}} qui liste les sous-pages d'une page.
#wfLoadExtension( 'Nuke' );

wfLoadExtension( 'ParserFunctions' );
$wgPFEnableStringFunctions = true; # Allows to activate the integrated string function functionality https://www.mediawiki.org/wiki/Special:MyLanguage/Extension:StringFunctions
$wgPFStringLengthLimit = 1000; # Defines the maximum length of a string that string functions are allowed to operate on. The default value is 1000.
#wfLoadExtension( 'PdfHandler' );
#wfLoadExtension( 'Poem' );
#wfLoadExtension( 'Renameuser' );
#wfLoadExtension( 'ReplaceText' );
wfLoadExtension( 'TabberNeue' );
$wgTabberNeueUseCodex=true;
$wgTabberNeueEnableAnimation=false;
$wgTabberNeueUpdateLocationOnTabChange=true;
#wfLoadExtension( 'Thanks' );
#wfLoadExtension( 'TitleBlacklist' );

# ---------------- For Lua module ----------------- #

wfLoadExtension( 'Scribunto' );
$wgScribuntoDefaultEngine = 'luastandalone';
$wgScribuntoUseGeSHi = true; // When Extension:SyntaxHighlight is installed, set this true to use it when displaying Module pages. (MediaWiki 1.30 or earlier.)
$wgScribuntoUseCodeEditor = true; // When Extension:CodeEditor is installed, set this true to use it when editing Module pages. (MediaWiki 1.30 or earlier.)
// $wgScribuntoEngineConf['luastandalone']['key'] = 'value';
#$wgScribuntoEngineConf['luastandalone']['luaPath'] = '/var/www/html/extensions/Scribunto/includes/Engines/LuaStandalone/binaries/lua5_1_5_linux_64_generic/lua'; // Specify the path to a Lua interpreter.
$wgScribuntoEngineConf['luastandalone']['errorFile'] = '/var/www/data/logs/lua-log.log';
$wgScribuntoEngineConf['luastandalone']['memoryLimit'] = 100*1024*1024; // (100 Mo) // Specify the memory limit in bytes for the standalone interpreter on Linux (enforced using ulimit).
$wgScribuntoEngineConf['luastandalone']['cpuLimit'] = 5.0; // (5s) // Specify the CPU time limit in seconds for the standalone interpreter on Linux (enforced using ulimit).
$wgScribuntoEngineConf['luastandalone']['allowEnvFuncs'] = false;// Set true to allow use of setfenv and getfenv in modules.

$wgScribuntoEngineConf['luasandbox']['profilerPeriod'] = 0.5; //
$wgScribuntoEngineConf['luasandbox']['memoryLimit'] = 100*1024*1024; // (100 Mo) // Specify the memory limit in bytes for the standalone interpreter on Linux (enforced using ulimit).
$wgScribuntoEngineConf['luasandbox']['cpuLimit'] = 5; // (5s) // Specify the CPU time limit in seconds for the standalone interpreter on Linux (enforced using ulimit).
$wgScribuntoEngineConf['luasandbox']['allowEnvFuncs'] = false;// Set true to allow use of setfenv and getfenv in modules.

wfLoadExtension( 'CodeEditor' ); // transforme les namespace de code (javscipt, css, module lua) en page de code et non en texte brut.
$wgDefaultUserOptions['usebetatoolbar'] = 1;// user option provided by WikiEditor extension
$wgCodeEditorEnableCore = true;							// To disable the editor on JavaScript and CSS pages in the MediaWiki, User and other core namespaces, set $wgCodeEditorEnableCore = false; (default is "true")
$wgScribuntoUseCodeEditor = true;						// To disable this extension for Scribunto, i.e. in the module namespace set $wgScribuntoUseCodeEditor = false; (default is "true")
wfLoadExtension( 'WikiEditor' );


wfLoadExtension( 'SyntaxHighlight_GeSHi' ); // met de la coloration syntaxique pour les code.
// usage : <syntaxhighlight lang="python" line start="55" highlight="1,4,8,10-15" inline>
#$wgSyntaxHighlightMaxLines
#$wgSyntaxHighlightMaxBytes // For performance reasons, blobs or pages (JS, Lua and CSS pages) larger than these values will not be highlighted. (since 1.40)
#$wgPygmentizePath (optional): Absolute path to pygmentize of the Pygments package. The extension bundles the Pygments package and $wgPygmentizePath points to the bundled version by default, but you can point to a different version, if you want to. For example: $wgPygmentizePath = "/usr/local/bin/pygmentize";


# End of automatically generated settings.
# Add more configuration options below.




# ——————————————————————————————————————————————————————————————
#                    Namespaces
# ——————————————————————————————————————————————————————————————


# —————————— namespace option —————————— #

# Personnal namespace :
# -- idées --
define("NS_IDEE", 110);
define("NS_IDEE_TALK", 111);
define("NS_TEMPS", 130);
define("NS_TEMPS_TALK", 131);
define("NS_OBJET", 160);
define("NS_OBJET_TALK", 161);
define("NS_LIEU", 170);
define("NS_LIEU_TALK", 171);


// Add namespaces.
$wgExtraNamespaces[NS_IDEE] = "Idée";
$wgExtraNamespaces[NS_IDEE_TALK] = "Discussion_Idée"; // Note underscores in the namespace name.
$wgNamespaceAliases['I'] = NS_IDEE;
$wgExtraNamespaces[NS_TEMPS] = "Chronologie";
$wgExtraNamespaces[NS_TEMPS_TALK] = "Discussion_Chronologie"; // Note underscores in the namespace name.
$wgNamespaceAliases['T'] = NS_TEMPS;
$wgExtraNamespaces[NS_OBJET] = "Objet";
$wgExtraNamespaces[NS_OBJET_TALK] = "Discussion_Objet"; // Note underscores in the namespace name.
$wgNamespaceAliases['O'] = NS_OBJET;
$wgExtraNamespaces[NS_LIEU] = "Lieu";
$wgExtraNamespaces[NS_LIEU_TALK] = "Discussion_Lieu"; // Note underscores in the namespace name.
$wgNamespaceAliases['L'] = NS_LIEU;


$wgNamespaceProtection[NS_IDEE]    = array( 'edit', 'editidée'  );
$wgNamespaceProtection[NS_OBJET]   = array( 'edit', 'editidée'  );
$wgNamespaceProtection[NS_LIEU]    = array( 'edit', 'editidée'  );
$wgNamespaceProtection[NS_TEMPS]   = array( 'edit', 'edittemps' );


# Namespace to search :
foreach([
	NS_MAIN, NS_HELP, NS_IDEE, NS_OBJET, NS_LIEU, NS_TEMPS
] as $ns)
	{ $wgNamespacesToBeSearchedDefault[$ns] = true; }


# Counted namespace in stat
$wgContentNamespaces = [
	NS_MAIN, NS_IDEE, NS_OBJET, NS_LIEU, NS_TEMPS
];



# Subpages Namespaces
foreach([
	NS_MAIN, NS_TALK,
	NS_CATEGORY, NS_CATEGORY_TALK,
	NS_TEMPLATE, NS_TEMPLATE_TALK,
	NS_PROJECT, NS_PROJECT_TALK,

	NS_IDEE, NS_IDEE_TALK,
	NS_TEMPS, NS_TEMPS_TALK,
	NS_OBJET, NS_OBJET_TALK,
	NS_LIEU, NS_LIEU_TALK
] as $ns)
	{ $wgNamespacesWithSubpages[$ns]=true; }



# ——————————————————————————————————————————————————————————————
#                         Group Permission
# ——————————————————————————————————————————————————————————————

# The following permissions were set based on your choice in the installer
#$wgGroupPermissions['*']['createaccount'] = false;
$wgGroupPermissions['*']['edit'] = false;
# Faire en sorte que les utilisateurs avec des adresses courriel confirmées soient dans le groupe.
$wgAutopromote['emailconfirmed'] = APCOND_EMAILCONFIRMED;
# Masquez le groupe de la liste des utilisateurs.
$wgImplicitGroups[] = 'emailconfirmed';

/*
	Boucle d'attribution des droits.
	Ce formalisme permet une configuration où pour chaque droit on indique la liste des utilisateurs autorisés
*/
foreach(['user', 'autoconfirmed','emailconfirmed', 'uploadaccess', 'sysop'] as $un) // boucle des types d'utilisateurs
	foreach([ // droit attribués au type d'utilisateur :
		'edit'					=> ['autoconfirmed', 'emailconfirmed', 'sysop'],
		'createpage'		=> ['autoconfirmed', 'emailconfirmed', 'sysop'],
		// téléversement
		'upload'				=> [ 'autoconfirmed', 'uploadaccess', 'sysop' ],
		// remplacement d'un fichier existant
		'reupload'			=> [ 'uploadaccess',  'sysop'],
		// télécharger depuis une url
		'upload_by_url'	=> [ 'uploadaccess',  'sysop'],
		// utilisation de l'interwiki
		'interwiki'			=> [ 'sysop' ],
		// droit d'édition des namespaces idée / lieu / objet
		'editidée'			=> [ 'emailconfirmed', 'sysop' ],
		// droit d'édition des namespaces temps
		'edittemps'			=> [ 'sysop' ],
	] as $droit => $tab)
		{ $wgGroupPermissions[$un][$droit] = in_array($un,$tab); }
