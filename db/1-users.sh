#!/bin/bash
set -e

# récupération variable en secret docker
WIKI_DB_USER=$(sed "s/'/''/g" $WIKI_DB_USER_FILE)
WIKI_DB_NAME=$(sed "s/'/''/g" $WIKI_DB_NAME_FILE)
WIKI_DB_PASSWORD=$(sed "s/'/''/g" $WIKI_DB_PASSWORD_FILE)

# création en utilisant les droits admin de la base de donnée et de son utilisateur "tout privilèges" avec mot de passe.
# note : $POSTGRES_USER et $POSTGRES_DB sont automatiquement déduit de $POSTGRES_USER_FILE et $POSTGRES_DB_FILE
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE USER "$WIKI_DB_USER" WITH ENCRYPTED PASSWORD '$WIKI_DB_PASSWORD';
	CREATE DATABASE "$WIKI_DB_NAME";
	GRANT ALL PRIVILEGES ON DATABASE "$WIKI_DB_NAME" TO "$WIKI_DB_USER";
EOSQL

echo "base de données créée"
