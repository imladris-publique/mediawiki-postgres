# Mediawiki-postgres

Ce dépôt est le code permettant de créer un docker mediawiki paramétré pour l'utilisation d'une base PostrgreSQL, de secrets docker et d'un serveur smtp distant.

C'est à partir de ce dépôt que nous fabriquons notre image docker des wiki dans le docker Hub.

Le docker-compose.yml ne sert qu'à builder le dépôt. Ses paramètres, notamment les secrets permet d'illustrer un usage avec des secrets docker.


Pour illustration, y est ajouté :
- un modèle de "LocalSettings"
- un modèle d'usage de secret docker
- un modèle d'usage de volume externe, pris dans le répertoire, où placer les extensions du wiki (mutualisable entre plusieurs wiki)
- un modèle d'usage dans le docker compose
- un modèle d'usage d'une base postrgresql dans le docker-compose.
- un modèle d'initialisation sécurisé de base dans le conteneur postgresql

## Méthode de déploiement du wiki

1. Préparer le fichier de configuration à partir du modèle.
  - penser à redéfinir les clefs du wiki : secret_key et upgrade_key
2. Préparer le fichier d'environnement wiki.env
	- préciser en particulier variable locale WIKI_URL (ou la _FILE correspondante) qui dépend de l'url choisie pour le site.
3. Le cas échéant, fabriquer un docker-compose.override.yml
4. Déployer sur le serveur grâce à docker-compose.
5. Passer par l'installateur de la BDD à l'url : http://{url du site}/mw-config/
  1. Utiliser le mot de passe upgrade_key
  2. Re-préciser comme dans la configuration seulement les infos de la première page de configuration,
    - ( le nom du wiki, le nom du namespace Projet, ainsi que les noms/mot de passe/mail de l'administrateur ).
  3. Demander à finir tout de suite. (le reste ne sert qu'à re-fabriquer la configuration)
6. Ne pas s'occuper du fichier de configuration proposé. Le wiki est installé.
